$(document).ready(function () {
  petition(null, 'POST', '/users/listar/', generarTabla)

  var ingresar = function () {
    // ejemplo de llamado al ajax jquery para POST insert
    petition({
      'name': $('#name').val(),
      'email': $('#email').val(),
      'gender': $('#gender').val(),
      'age': $('#age').val()
    }, 'POST', '/users/',
    respuestaCallback)
  }
  $('#boton').click(ingresar) // (1)
})

function generarTabla (salida) {
  if (salida !== 'ERROR') {
  // clear table
    $('table>tbody').find('tr').remove()
    // show array
    for (var i = 0; i < salida.length; i++) {
      if (salida[i] !== null && salida[i].name !== undefined) {
        var eliminar = '<button type="button" onClick="eventDelete(&#39;' + salida[i]._id + '&#39;)" class="btn btn-danger">Eliminar</button>'
        var modificar = '<button type="button" onClick="eventEdit(&#39;' + salida[i]._id + '&#39;)" class="btn btn btn-success">Modificar</button>'
        var filas =    
                        '<td>' + salida[i].name + '</td>' +
                        '<td>' + salida[i].email + '</td>' +
                        '<td>' + salida[i].gender + '</td>' +
                        '<td>' + salida[i].age + '</td>' +
                        '<td>' + modificar + '</td>' +
                        '<td>' + eliminar + '</td>'
        var html = '<tr>' + filas + '</tr>'
        // write html
        $('table>tbody').append(html)
      }
    }
  }
}

function petition (info, metod, ruta, callback) {
  $.ajax({
    url: ruta,
    type: metod,
    data: info
  }).done(callback)
}
function eventDelete (id) {
  console.log(id)
  petition({
    'id': id
  }, 'DELETE', '/users/',
  respuestaCallback)
}
function eventEdit (id) {
  petition(null, 'POST', '/users/' + id, mostrarData)
}
function mostrarData (salida) {
  console.log(salida.name)
  $('#name').val(salida.name)
  $('#email').val(salida.email)
  $('#gender').val(salida.gender)
  $('#age').val(salida.age)
}
function respuestaCallback (salida) {
  petition(null, 'POST', '/users/listar/', generarTabla)
}
